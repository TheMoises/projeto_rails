Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :meal_categories
  get 'meal_categories' => 'meal_categories#index'
  post 'meal_categories/create' => 'meal_categories#create'
  delete 'meal_categories/delete' => 'meal_categories#destroy'
  put 'meal_categories/id' => 'meal_categories#update'

  resources :meals
  get 'meals' => 'meals#index'
  post 'meals/create' => 'meals#create'
  delete 'meals/delete' => 'meals#destroy'
  put 'meals/id' => 'meals#update'

  resources :orders
  get 'orders' => 'orders#index'
  post 'orders/create' => 'orders#create'
  delete 'orders/delete' => 'orders#destroy'
  put 'orders/id' => 'orders#update'
  resources :users 
end
