class MealCategory < ApplicationRecord
  has_many :meals
  validates_associated :meals

  validates :name, length: {maximum: 45}
  validates :name, presence: true
  validates :name, uniqueness: true
end
 