class Situation < ApplicationRecord
  has_many :orders
  validates_associates :orders

  validates :description, presence: true
  validates :description, length: { maximum: 45 }
end