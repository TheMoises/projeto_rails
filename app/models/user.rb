class User < ApplicationRecord
  has_many :orders
  validates_associated :orders

  validates :name, :email, :password, presence: true
  validates :name, length: {in 3..45}

  validates :email, length: {maximum: 45}
  validates :email, confirmation: true
  validates :email, uniqueness: true

  validates :password, length: {in 6..45}
  validates :password, confirmation: true
  
  validates :admin, inclusion: {in: [false, true]}
  validates :admin, exclusion: {in: [nil]}
end

