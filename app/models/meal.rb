class Meal < ApplicationRecord
  has_many :meals
  validates_associated :meals
  has_one :mealCategory
  validates_associated :mealCategory
  
  validates :name, :description, :price, :mealCategory, presence: true
  validates :name, length: { maximum: 45 }
  validates :name, uniqueness: true
  validates :description, length: { maximum: 45 }  
  validates :price, numericality: true
  validates :available, length: { maximun: 45 }

end