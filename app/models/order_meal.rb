class OrderMeal < ApplicationRecord
  has_one :meal
  validates_associated :meal
  has_one :order
  validates_associated :order

  validates :quantity, :order_id, :meal_id, presence: true

  validates :quantity, numericality: true;
end