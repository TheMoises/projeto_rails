class Order < ApplicationRecord
  has_many :order_meals
  validates_associated :order_meals
  has_one :user
  validates_associated :user
  has_one :situation
  validates_associated :situation

  validates :price, :user_id, :situation_id, presence: true

  validates :price, numericality: true
end