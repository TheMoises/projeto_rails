class OrdersController < ApplicationController
    def index
        @orders = Order.all
        render json: @orders
    end

    def create
        @orders = Order.new(order_params)

        @orders.save
        render json @orders, status: :created
    end

    def update
        @orders.update(order_params)
        render json: @orders
    end 

    def destroy
        @orders = @orders.where(id: params[:id]).first
        if @orders.destroy
            head(:ok)
        else 
            head(:unprocessable_entity)
        end
    end

    private

    def order_params
        params.require(:order).permit(:price, :user_id, :situation_id)
    end
end
