class MealsController < ApplicationController
    def index
        @meals = Meal.all
        render json: @meals
    end

    def create
        @meals = Meal.new(meal_params)

        @meals.save
        render json @meals, status: :created
    end

    def update
        @meals.update(meal_params)
        render json: @meals
    end 

    def destroy
        @meals = @meals.where(id: params[:id]).first
        if @meals.destroy
            head(:ok)
        else 
            head(:unprocessable_entity)
        end
    end

    private

    def meal_params
        params.require(:meal).permit(:name, :description, :price, :available, :mealCategory_id)
    end
end
