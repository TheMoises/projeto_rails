class MealCategoriesController < ApplicationController
  def index
    @meal_categories = MealCategory.all
    render json: @meal_categories
  end  

  def create
    @meal_categories = MealCategory.new(meal_category_params)

    @meal_categories.save
    render json @meal_categories, status: :created
  end

  def update
    @meal_categories.update(meal_category_params)
    render json: @meal_categories
  end 

  def destroy
    @meal_categories = @meal_categories.where(id: params[:id]).first
    if @meal_categories.destroy
      head(:ok)
    else 
      head(:unprocessable_entity)
    end
  end

  private

  def meal_category_params
    params.require(:meal_category).permit(:name)
  end
end
