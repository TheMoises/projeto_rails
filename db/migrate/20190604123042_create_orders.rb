class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.float :price
      t.references :user_id
      t.references :situation_id

      t.timestamps
    end
  end
end
