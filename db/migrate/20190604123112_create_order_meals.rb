class CreateOrderMeals < ActiveRecord::Migration[5.2]
  def change
    create_table :order_meals do |t|
      t.integer :quantity
      t.references :order_id
      t.references :meal_id

      t.timestamps
    end
  end
end
