class CreateMeals < ActiveRecord::Migration[5.2]
  def change
    create_table :meals do |t|
      t.string :name
      t.text :description
      t.string :price
      t.string :available
      t.references :mealCategory

      t.timestamps
    end
  end
end
